package LeapYear;


public class LeapYear {

	public boolean isDividable4(int year){
		return year % 4 == 0;
	}
	
	public boolean isDividable100(int year){
		return year % 100 == 0;
	}
	
	public boolean isDividable400(int year){
		return year % 400 == 0;
	}
		
	public boolean isLeapYear(int year) {
		//return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
		return isDividable4(year) && !isDividable100(year) || isDividable400(year);
	}

}
