package LeapYear;


import static org.junit.Assert.*;

import org.junit.Test;

import LeapYear.LeapYear;

public class TestLeapYear {

	@Test
	public void testIsDividable4() throws Exception {
		LeapYear testable = new LeapYear();
		assertTrue(testable.isDividable4(2004));
	}
	
	@Test
	public void testNotLeapWhenDividable100() throws Exception {
		LeapYear testable = new LeapYear();
		assertFalse( !testable.isDividable100(1900) );
	}
	
	@Test
	public void testLeapYearWhenDividable400() throws Exception {
		LeapYear testable = new LeapYear();
		assertTrue(testable.isDividable400(2000));
	}
	
	@Test
	public void testLeapYearRule1and2() throws Exception {
		LeapYear testable = new LeapYear();
		int year = 1996;
		assertTrue(testable.isDividable4(year) && !testable.isDividable100(year));
	}
	
	@Test
	public void testIsLeapYear() throws Exception {
		LeapYear testable = new LeapYear();		
		assertTrue(testable.isLeapYear(2008));
	}
	
}
